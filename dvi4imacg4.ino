// Using bounce from http://www.arduino.cc/playground/Code/Bounce
#include <Bounce.h>

const uint8_t pinInPowerButton = 9;        // Connected to a button (other end of the button should be wired to ground)
const uint8_t pinInDVIPower = 8;           // Connected via 5K Ohm to DVI pin 14
const uint8_t pinOutDVIHotPlugDetect = A6; // Connected via 1K Ohm to DVI pin 16
const uint8_t pinOutPinkOnGrey = 6;        // Connected directly to the pink on gray cable
const uint8_t pinOutInverterOnPower = 5;   // Connected via 10K Ohm to yellow inverter cable
const uint8_t pinOutInverterDimPWM = 3;    // Connected via 10K Ohm to orange inverter cable
const uint8_t pinOutLedPwm = 10;           // Connected via 220 Ohm to the LED red cable (the other one (green) is connected to ground)) 
const uint8_t pinOut24VoltOn = A5;         // Connected via 1K Ohm to the original PSU white cable

const uint8_t pinInTrimPot1 = A4;          // Intend to use this for screen dim
const uint8_t pinInTrimPot2 = A7;          // Intend to use this for fan pwm

bool power24voltState = LOW;
long lastTime = 0;

/**
 *
 * This function is from http://playground.arduino.cc/Code/PwmFrequency
 *
 * Divides a given PWM pin frequency by a divisor.
 *
 * The resulting frequency is equal to the base frequency divided by
 * the given divisor:
 *   - Base frequencies:
 *      o The base frequency for pins 3, 9, 10, and 11 is 31250 Hz.
 *      o The base frequency for pins 5 and 6 is 62500 Hz.
 *   - Divisors:
 *      o The divisors available on pins 5, 6, 9 and 10 are: 1, 8, 64,
 *        256, and 1024.
 *      o The divisors available on pins 3 and 11 are: 1, 8, 32, 64,
 *        128, 256, and 1024.
 *
 * PWM frequencies are tied together in pairs of pins. If one in a
 * pair is changed, the other is also changed to match:
 *   - Pins 5 and 6 are paired on timer0
 *   - Pins 9 and 10 are paired on timer1
 *   - Pins 3 and 11 are paired on timer2
 *
 * Note that this function will have side effects on anything else
 * that uses timers:
 *   - Changes on pins 3, 5, 6, or 11 may cause the delay() and
 *     millis() functions to stop working. Other timing-related
 *     functions may also be affected.
 *   - Changes on pins 9 or 10 will cause the Servo library to function
 *     incorrectly.
 *
 * Thanks to macegr of the Arduino forums for his documentation of the
 * PWM frequency divisors. His post can be viewed at:
 *   http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1235060559/0#4
 */
void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}

class Periodical {
  protected:
  unsigned long _executePeriod;
  unsigned long _lastTime;
  
  Periodical(unsigned long inExecutePeriod){
    _executePeriod = inExecutePeriod;
    _lastTime = 0;
  }

  virtual void execute() = 0;

  public: 
  bool update() {
    unsigned long now = millis();
    if (now - _lastTime > _executePeriod) {
      _lastTime = now;
      execute();
      return true;
    } 
    return false;
  }
};

class Fader: public Periodical {
 enum StateType {
  on = 0,
  off,
  pulsing
 };
 
 float _fadePeriod;
 StateType _state;
 int _pinOut;
 
 public:
  Fader(unsigned long inExecutePeriod, unsigned long inFadePeriod, int pinOut):Periodical(inExecutePeriod){
    _fadePeriod = inFadePeriod;
    _state = off;
    _pinOut = pinOut;
    pinMode(_pinOut,OUTPUT);
  }
  
  void turnOn() {
    _state = on;
    analogWrite(_pinOut,255);
  }
 
  void turnOff() {
    _state = off;
    analogWrite(_pinOut,0);
  }
  
  void turnPulsing() {
    _state = pulsing;
  }
  
  void turnPulsing(unsigned long inFadePeriod) {
    _fadePeriod = inFadePeriod;
    _state = pulsing;
  }
  
 protected: 
  virtual void execute() {
    if (pulsing == _state) {
      byte val = 128.0+127.0*cos(2*PI/_fadePeriod*_lastTime);
      analogWrite(_pinOut,val);
      //Serial.println(val);
    }
  }
}; 

Fader ledFader(20, 2000, pinOutLedPwm);

// Instantiate a Bounce object with a 5 millisecond debounce time
Bounce bouncerPowerButton = Bounce( pinInPowerButton, 5 ); 
Bounce bouncerDviPower = Bounce( pinInDVIPower, 5 ); 

void setup() {
  
  // Inputs
  pinMode(pinInPowerButton,INPUT_PULLUP);
  pinMode(pinInDVIPower,INPUT);  
  pinMode(pinInTrimPot1,INPUT);  
  pinMode(pinInTrimPot2,INPUT);  
  
  // Outputs
  pinMode(pinOut24VoltOn,OUTPUT);
  pinMode(pinOutDVIHotPlugDetect,OUTPUT);
  pinMode(pinOutPinkOnGrey,OUTPUT);
  pinMode(pinOutInverterOnPower,OUTPUT);
  pinMode(pinOutInverterDimPWM,OUTPUT);

  digitalWrite(pinOutInverterOnPower,LOW);
  digitalWrite(pinOut24VoltOn,LOW);
  digitalWrite(pinOutDVIHotPlugDetect,LOW);
  digitalWrite(pinOutPinkOnGrey,LOW);
  digitalWrite(pinOutInverterDimPWM,LOW);

  Serial.begin(9600); 
}

void loop() {
  
  // Get the update value
  if (bouncerPowerButton.update() && bouncerPowerButton.fallingEdge() ){
    power24voltState = !power24voltState;
    digitalWrite(pinOut24VoltOn, power24voltState );
  }
  
  bouncerDviPower.update();
  ledFader.update();
  
  if (millis() - lastTime > 400) {
    bool powerOnState = bouncerDviPower.read() && power24voltState;
    if (powerOnState){
      ledFader.turnOff();
    } else {
      ledFader.turnPulsing();
    }
    
    digitalWrite(pinOutInverterOnPower,powerOnState);
    digitalWrite(pinOutDVIHotPlugDetect, powerOnState);
    digitalWrite(pinOutPinkOnGrey, powerOnState);

    lastTime = millis();
    Serial.print("DIV power:");
    Serial.print(bouncerDviPower.read());
    Serial.print(" power:");
    Serial.print(power24voltState);
    Serial.print(" displayOn:");
    Serial.print(powerOnState);
    Serial.print(" analog1:\t");
    int value = analogRead(pinInTrimPot1);
    Serial.print(value);
    Serial.print("\tanalog1:\t");
    value = analogRead(pinInTrimPot2);
    Serial.println(value);
    //analogWrite(pinOutInverterDimPWM,map(value, 0,1023,0,255));
  }
}

